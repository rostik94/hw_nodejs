const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'A user must have a name'],
    trim: true,
    minlength: [2, 'A user name must have more or equal then 2 characters'],
    maxlength: [25, 'A user name must have less or equal then 25 characters'],
  },
  surname: {
    type: String,
    required: [true, 'A user must have a surname'],
    trim: true,
    minlength: [2, 'Surname must have more or equal then 2 characters'],
    maxlength: [25, 'Surname must have less or equal then 25 characters'],
  },
  age: {
    type: Number,
    required: [true, 'A user must have an age'],
    min: [18, 'A user must have 18 years or more'],
    max: [150, 'It is not possible no be so old'],
  },
  phoneNumber: {
    type: Number,
    required: [true, 'A user must have a phone number'],
  },
  email: {
    type: String,
    required: [true, 'A user must have an email'],
    unique: true,
  },
  city: {
    type: String,
    required: [true, 'A user must have a city'],
    trim: true,
  },
  country: {
    type: String,
    required: [true, 'A user must have a country'],
    trim: true,
  },
  married: Boolean,
  company: {
    type: String,
    trim: true,
  },
  position: {
    type: String,
    trim: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const User = mongoose.model('User', userSchema);

module.exports = User;
